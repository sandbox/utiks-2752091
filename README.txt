This is a very light-weight module allows additional permissions to be created and managed through a administration form.

It uses the route permissions system to allow or disalow access to it.

On the administration page a user is able to create a permission with name and path(s).

These permissions can then be assigned to roles on the permissions page.
